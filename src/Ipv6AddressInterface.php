<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-ip-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Ip;

use Stringable;

/**
 * Ipv6AddressInterface interface file.
 *
 * This interface represents an ipv6.
 *
 * @author Anastaszor
 * @see https://www.ietf.org/rfc/rfc2460.txt
 */
interface Ipv6AddressInterface extends Stringable
{
	
	/**
	 * Gets the first group.
	 *
	 * @return integer between 0 and 65535
	 */
	public function getFirstGroup() : int;
	
	/**
	 * Gets the second group.
	 *
	 * @return integer between 0 and 65535
	 */
	public function getSecondGroup() : int;
	
	/**
	 * Gets the third group.
	 *
	 * @return integer between 0 and 65535
	 */
	public function getThirdGroup() : int;
	
	/**
	 * Gets the fourth group.
	 *
	 * @return integer between 0 and 65535
	 */
	public function getFourthGroup() : int;
	
	/**
	 * Gets the fifth group.
	 *
	 * @return integer between 0 and 65535
	 */
	public function getFifthGroup() : int;
	
	/**
	 * Gets the sixth group.
	 *
	 * @return integer between 0 and 65535
	 */
	public function getSixthGroup() : int;
	
	/**
	 * Gets the seventh group.
	 *
	 * @return integer between 0 and 65535
	 */
	public function getSeventhGroup() : int;
	
	/**
	 * Gets the eigth group.
	 *
	 * @return integer between 0 and 65535
	 */
	public function getEighthGroup() : int;
	
	/**
	 * Gets the short representation of this ip address.
	 *
	 * @return string
	 */
	public function getShortRepresentation() : string;
	
	/**
	 * Gets the canonical representation of this ip address.
	 *
	 * @return string
	 */
	public function getCanonicalRepresentation() : string;
	
	/**
	 * Gets whether this ip address is equal to another ip address.
	 *
	 * @param null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object>> $object
	 * @return boolean
	 */
	public function equals($object) : bool;
	
	/**
	 * Does the bitwise AND operation for each bit of this address, using the
	 * other given address.
	 *
	 * @param Ipv6AddressInterface $other
	 * @return Ipv6AddressInterface the result
	 */
	public function bitwiseAnd(Ipv6AddressInterface $other) : Ipv6AddressInterface;
	
	/**
	 * Does the bitwise OR operation for each bit of this address, using the
	 * other given address.
	 *
	 * @param Ipv6AddressInterface $other
	 * @return Ipv6AddressInterface the result
	 */
	public function bitwiseOr(Ipv6AddressInterface $other) : Ipv6AddressInterface;
	
	/**
	 * Does the bitwise XOR operation for each bit of this address, using the
	 * other given address.
	 *
	 * @param Ipv6AddressInterface $other
	 * @return Ipv6AddressInterface the result
	 */
	public function bitwiseXor(Ipv6AddressInterface $other) : Ipv6AddressInterface;
	
	/**
	 * Does the bitwise NOT operation for each bit of this address.
	 *
	 * @return Ipv6AddressInterface the result
	 */
	public function bitwiseNot() : Ipv6AddressInterface;
	
	/**
	 * Does the addition of this address with the given address. Each byte adds
	 * up separately with remainders. No address may add upper than the
	 * ffff:ffff:ffff:ffff:ffff:ffff:ffff:ffff address.
	 *
	 * @param Ipv6AddressInterface $other
	 * @return Ipv6AddressInterface the result
	 */
	public function add(Ipv6AddressInterface $other) : Ipv6AddressInterface;
	
	/**
	 * Does the substraction of this address with the given address. Each byte
	 * substracts up separately with remainteds. No adderss may substract lower
	 * than the 0000:0000:0000:0000:0000:0000:0000:0000 address.
	 *
	 * @param Ipv6AddressInterface $other
	 * @return Ipv6AddressInterface the result
	 */
	public function substract(Ipv6AddressInterface $other) : Ipv6AddressInterface;
	
	/**
	 * Gets a representation of an ipv6 with 8 integers.
	 *
	 * @return array<integer, integer>
	 */
	public function toArray() : array;
	
	/**
	 * Gets the ipv4 encapsulated into this ipv6.
	 * 
	 * @return Ipv4AddressInterface
	 */
	public function toIpv4() : Ipv4AddressInterface;
	
}
