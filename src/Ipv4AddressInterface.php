<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-ip-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Ip;

use Stringable;

/**
 * Ipv4AddressInterface interface file.
 *
 * This interface represents an ipv4.
 *
 * @author Anastaszor
 * @see https://www.ietf.org/rfc/rfc791.txt
 */
interface Ipv4AddressInterface extends Stringable
{
	
	/**
	 * Gets the first byte of this ip address.
	 *
	 * @return integer
	 */
	public function getFirstByte() : int;
	
	/**
	 * Gets the second byte of this ip address.
	 *
	 * @return integer
	 */
	public function getSecondByte() : int;
	
	/**
	 * Gets the third byte of this ip address.
	 *
	 * @return integer
	 */
	public function getThirdByte() : int;
	
	/**
	 * Gets the fourth byte of this ip address.
	 *
	 * @return integer
	 */
	public function getFourthByte() : int;
	
	/**
	 * This function returns the exact unsigned 32 bit integer that corresponds
	 * to this ip address.
	 *
	 * @return integer
	 */
	public function getSignedValue() : int;
	
	/**
	 * Gets the short representation of this ip address.
	 *
	 * @return string
	 */
	public function getShortRepresentation() : string;
	
	/**
	 * Gets the canonical representation of this ip address.
	 *
	 * @return string
	 */
	public function getCanonicalRepresentation() : string;
	
	/**
	 * Gets whether this ip address is equal to another ip address.
	 *
	 * @param null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object>> $object
	 * @return boolean
	 */
	public function equals($object) : bool;
	
	/**
	 * Does the bitwise AND operation for each bit of this address, using the
	 * other given address.
	 *
	 * @param Ipv4AddressInterface $other
	 * @return Ipv4AddressInterface the result
	 */
	public function bitwiseAnd(Ipv4AddressInterface $other) : Ipv4AddressInterface;
	
	/**
	 * Does the bitwise OR operation for each bit of this address, using the
	 * other given address.
	 *
	 * @param Ipv4AddressInterface $other
	 * @return Ipv4AddressInterface the result
	 */
	public function bitwiseOr(Ipv4AddressInterface $other) : Ipv4AddressInterface;
	
	/**
	 * Does the bitwise XOR operation for each bit of this address, using the
	 * other given address.
	 * 
	 * @param Ipv4AddressInterface $other
	 * @return Ipv4AddressInterface the result
	 */
	public function bitwiseXor(Ipv4AddressInterface $other) : Ipv4AddressInterface;
	
	/**
	 * Does the bitwise NOT operation for each bit of this address.
	 *
	 * @return Ipv4AddressInterface the result
	 */
	public function bitwiseNot() : Ipv4AddressInterface;
	
	/**
	 * Does the addition of this address with the given address. Each byte adds
	 * up separately with remainders. No address may add upper than the
	 * 255.255.255.255 address.
	 *
	 * @param Ipv4AddressInterface $other
	 * @return Ipv4AddressInterface the result
	 */
	public function add(Ipv4AddressInterface $other) : Ipv4AddressInterface;
	
	/**
	 * Does the substraction of this address with the given address. Each byte
	 * substracts up separately with remainteds. No adderss may substract lower
	 * than the 0.0.0.0 address.
	 *
	 * @param Ipv4AddressInterface $other
	 * @return Ipv4AddressInterface the result
	 */
	public function substract(Ipv4AddressInterface $other) : Ipv4AddressInterface;
	
	/**
	 * Gets a representation of an ipv4 with 4 integers.
	 * 
	 * @return array<integer, integer>
	 */
	public function toArray() : array;
	
	/**
	 * Encapsulates this ipv4 into a canonical ipv6 address.
	 * 
	 * @return Ipv6AddressInterface
	 * @see https://tools.ietf.org/html/rfc6052
	 */
	public function toIpv6() : Ipv6AddressInterface;
	
}
