<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-ip-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Ip;

use Stringable;

/**
 * Ipv4NetworkInterface interface file.
 *
 * This interface represents a network of ipv4 adresses.
 *
 * @author Anastaszor
 */
interface Ipv4NetworkInterface extends Stringable
{
	
	/**
	 * Gets the ipv4 which starts the network range.
	 *
	 * @return Ipv4AddressInterface
	 */
	public function getStartIp() : Ipv4AddressInterface;
	
	/**
	 * Gets the ipv4 which ends the network range.
	 *
	 * @return Ipv4AddressInterface
	 */
	public function getEndIp() : Ipv4AddressInterface;
	
	/**
	 * Gets the ipv4 which known as the network address.
	 *
	 * @return Ipv4AddressInterface
	 */
	public function getNetworkIp() : Ipv4AddressInterface;
	
	/**
	 * Gets the ipv4 which represents the subnet mask.
	 *
	 * @return Ipv4AddressInterface
	 */
	public function getNetmaskIp() : Ipv4AddressInterface;
	
	/**
	 * Gets the ipv4 which represents the inverse subnet mask (known as
	 * wildcard mask).
	 *
	 * @return Ipv4AddressInterface
	 */
	public function getWildmaskIp() : Ipv4AddressInterface;
	
	/**
	 * Gets the number of bits that are taken by the mask.
	 *
	 * @return integer
	 */
	public function getMaskBits() : int;
	
	/**
	 * Gets the gateway ipv4 address.
	 *
	 * @return Ipv4AddressInterface
	 */
	public function getGatewayIp() : Ipv4AddressInterface;
	
	/**
	 * Gets the broadcast ipv4 address.
	 *
	 * @return Ipv4AddressInterface
	 */
	public function getBroadcastIp() : Ipv4AddressInterface;
	
	/**
	 * Gets the number of addresses that are available in this network.
	 *
	 * @return integer
	 */
	public function getNumberOfAddresses() : int;
	
	/**
	 * Gets the network class of this network.
	 * Networks of class A have their first octet like 0xxxxxxx
	 *          of class B have their first octet like 10xxxxxx
	 *          of class C have their first octet like 110xxxxx
	 *          of class D have their first octet like 1110xxxx
	 *          of class E are the rest.
	 *
	 * @return string
	 */
	public function getNetworkClass() : string;
	
	/**
	 * Gets whether this network equals the other object.
	 * 
	 * @param null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object>> $other
	 * @return boolean
	 */
	public function equals($other) : bool;
	
	/**
	 * Gets whether given ipv4 is included in this network.
	 *
	 * @param Ipv4AddressInterface $address
	 * @return boolean
	 */
	public function containsAddress(Ipv4AddressInterface $address) : bool;
	
	/**
	 * Gets whether given network is included in this network.
	 *
	 * @param Ipv4NetworkInterface $subnetwork
	 * @return boolean
	 */
	public function containsNetwork(Ipv4NetworkInterface $subnetwork) : bool;
	
	/**
	 * Gets the smallest network that contains this network and the other given
	 * address.
	 * 
	 * @param Ipv4AddressInterface $address
	 * @return Ipv4NetworkInterface
	 */
	public function absorbAddress(Ipv4AddressInterface $address) : Ipv4NetworkInterface;
	
	/**
	 * Gets the smallest network that contains this network and the other given
	 * network.
	 * 
	 * @param Ipv4NetworkInterface $network
	 * @return Ipv4NetworkInterface
	 */
	public function absorbNetwork(Ipv4NetworkInterface $network) : Ipv4NetworkInterface;
	
	/**
	 * Gets a canonical string representation of this network.
	 *
	 * @return string
	 */
	public function getCanonicalRepresentation() : string;
	
	/**
	 * Gets a representation of an ipv4 network with 5 integers.
	 *
	 * @return array<integer, integer>
	 */
	public function toArray() : array;
	
}
