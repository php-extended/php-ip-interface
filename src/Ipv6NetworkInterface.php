<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-ip-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Ip;

use Stringable;

/**
 * Ipv6NetworkInterface interface file.
 * 
 * This interface represents a network of ipv6 adresses.
 * 
 * @author Anastaszor
 */
interface Ipv6NetworkInterface extends Stringable
{
	
	/**
	 * Gets the ipv6 which starts the network range.
	 *
	 * @return Ipv6AddressInterface
	 */
	public function getStartIp() : Ipv6AddressInterface;
	
	/**
	 * Gets the ipv6 which ends the network range.
	 *
	 * @return Ipv6AddressInterface
	 */
	public function getEndIp() : Ipv6AddressInterface;
	
	/**
	 * Gets the ipv6 which known as the network address.
	 *
	 * @return Ipv6AddressInterface
	 */
	public function getNetworkIp() : Ipv6AddressInterface;
	
	/**
	 * Gets the ipv6 which represents the subnet mask.
	 *
	 * @return Ipv6AddressInterface
	 */
	public function getNetmaskIp() : Ipv6AddressInterface;
	
	/**
	 * Gets the ipv6 which represents the inverse subnet mask (known as
	 * wildcard mask).
	 *
	 * @return Ipv6AddressInterface
	 */
	public function getWildmaskIp() : Ipv6AddressInterface;
	
	/**
	 * Gets the number of bits that are taken by the mask.
	 *
	 * @return integer
	 */
	public function getMaskBits() : int;
	
	/**
	 * Gets the gateway ipv6 address.
	 *
	 * @return Ipv6AddressInterface
	 */
	public function getGatewayIp() : Ipv6AddressInterface;
	
	/**
	 * Gets the broadcast ipv6 address.
	 *
	 * @return Ipv6AddressInterface
	 */
	public function getBroadcastIp() : Ipv6AddressInterface;
	
	/**
	 * Gets the number of addresses that are available in this network.
	 *
	 * @return integer
	 */
	public function getNumberOfAddresses() : int;
	
	/**
	 * Gets whether this network equals the other object.
	 *
	 * @param null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object>> $other
	 * @return bool
	 */
	public function equals($other) : bool;
	
	/**
	 * Gets whether given ipv6 is included in this network.
	 *
	 * @param Ipv6AddressInterface $address
	 * @return boolean
	 */
	public function containsAddress(Ipv6AddressInterface $address) : bool;
	
	/**
	 * Gets whether given network is included in this network.
	 *
	 * @param Ipv6NetworkInterface $subnetwork
	 * @return boolean
	 */
	public function containsNetwork(Ipv6NetworkInterface $subnetwork) : bool;
	
	/**
	 * Gets the smallest network that contains this network and the other given
	 * address.
	 *
	 * @param Ipv6AddressInterface $address
	 * @return Ipv6NetworkInterface
	 */
	public function absorbAddress(Ipv6AddressInterface $address) : Ipv6NetworkInterface;
	
	/**
	 * Gets the smallest network that contains this network and the other given
	 * network.
	 *
	 * @param Ipv6NetworkInterface $network
	 * @return Ipv6NetworkInterface
	 */
	public function absorbNetwork(Ipv6NetworkInterface $network) : Ipv6NetworkInterface;
	
	/**
	 * Gets a canonical string representation of this network.
	 *
	 * @return string
	 */
	public function getCanonicalRepresentation() : string;
	
	/**
	 * Gets a representation of an ipv6 network with 9 integers.
	 *
	 * @return array<integer, integer>
	 */
	public function toArray() : array;
	
}
