<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-ip-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Ip;

use PhpExtended\Parser\ParserInterface;

/**
 * Ipv4AddressParserInterface interface file.
 *
 * This class represents a parser for ip addresses.
 *
 * @author Anastaszor
 * @extends \PhpExtended\Parser\ParserInterface<Ipv4NetworkInterface>
 */
interface Ipv4NetworkParserInterface extends ParserInterface
{
	
	// nothing to add
	// php does not accepts covariant return types between interfaces
	
}
